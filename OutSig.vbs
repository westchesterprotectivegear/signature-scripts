'========================== 
' 
' ############### Script that creates a standard company signature for all users (in a corporate environment); pulling data from Active Directory - dynamic logo insertion depending on department field if needed. 
' 
' Original by Severn Dickinson 
'
' Big up to Script Centre 
' 
' 17-01-2011. 
' 
'=========================== 
 
On Error Resume Next 
Set objSysInfo = CreateObject("ADSystemInfo") 
 
 
' ########### This section connects to Active Directory as the currently logged on user 
strUser = objSysInfo.UserName 
Set objUser = GetObject("LDAP://" & strUser)  
 
 
' ########### This section sets up the variables we want to call in the script (items on the left; whereas the items on the right are the active directory database field names) - ie strVariablename = objuser.ad.databasename 
strGiven = objuser.givenName
If Len(objuser.initials) = 1 then
	strInitials = objuser.initials & ". "
end if
strSurname = objuser.sn 
strAddress1 = objUser.streetaddress 
strAddress1EXT = objUser.postofficebox 
strCity = objuser.l 
strRegion = objuser.st 
strPostcode = objuser.postalcode 
strCountry = objuser.c 
strFax = objuser.facsimileTelephoneNumber 
strMobile = objuser.mobile 
strTitle = objUser.Title 
strDepartment = objUser.Department 
strCompany = objUser.Company 
strPhone = objUser.telephoneNumber 
strEmail =objuser.mail 
strWeb = objuser.wWWHomePage 
strNotes = objuser.info 
strExt = objuser.ipPhone 
strDDI = objuser.homephone
strSkype =objuser.pager 
strAssistant = objUser.Assistant
strEmailTEXT = "Email: " 
strAccName = objUser.sAMAccountName
strPostNominal = objUser.extensionAttribute1

' ########### Map Network Drives to PC
Dim objNetwork, strLocalDrive, strRemoteShare
Set objNetwork = WScript.CreateObject("WScript.Network")
strLocalDrive = "H:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Users\" & sAMAccountName
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

strLocalDrive = "P:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Public"
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

Set colGroups = objuser.groups
For Each objGroup in colGroups
	If objGroup.cn = "KAM" Then
		strLocalDrive = "G:"
		strRemoteShare = "\\wcmktg.com\WCPG HQ\Design"
		'objNetwork.RemoveNetworkDrive strLocalDrive
		objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True
	End if
Next		
 
 
' ########### Sets up word template 
Set objWord = CreateObject("Word.Application") 
Set objDoc = objWord.Documents.Add() 
Set objSelection = objWord.Selection 
objSelection.Style = "No Spacing" 
Set objEmailOptions = objWord.EmailOptions 
Set objSignatureObject = objEmailOptions.EmailSignature 
Set objSignatureEntries = objSignatureObject.EmailSignatureEntries 
 
 
' ########### Calls the variables from above section and inserts into word template, also sets initial font typeface, colour etc. 
objSelection.Font.Name = "Arial" 
objSelection.Font.Size = 11 
objselection.Font.Bold = false 
objSelection.Font.Color = RGB (000,000,000) 
if strAccName = "JKastan" then
	objSelection.typetext "Best Regards,"
	objSelection.TypeParagraph()
end if
objSelection.ParagraphFormat.Alignment = 0
objSelection.TypeParagraph()
objSelection.Font.Bold = True 
objSelection.TypeText strGiven & " " & strinitials  & strSurname & strPostNominal & " | " & strTitle
'objSelection.TypeParagraph()
objSelection.ParagraphFormat.Alignment = 0

' #########  Add address from user account.
objSelection.TypeParagraph()
objSelection.Font.Bold = True
objSelection.Font.Size = 10 
objSelection.TypeText strAddress1 & " | " & strCity & ", " & strRegion & " " & strPostCode

objSelection.TypeParagraph()
objSelection.Font.Bold = True
objSelection.Font.Size = 10 
objSelection.TypeText "Office: "
objSelection.Font.Bold = False
objSelection.TypeText "800.647.1900 ext. " & strExt

' ######## Add Direct dial number if available
If Len(strPhone) > 1 then
	objSelection.Font.Bold = True
	objSelection.Font.Size = 10 
	objSelection.TypeText " | Direct: "
	objSelection.Font.Bold = False
	objSelection.TypeText strPhone
end if

' ######## Add Mobile number if available
If Len(strMobile) > 1 then
	objSelection.Font.Bold = True
	objSelection.Font.Size = 10 
	objSelection.TypeText " | Mobile: "
	objSelection.Font.Bold = False
	objSelection.TypeText strMobile
end if

' ######## Add Fax number if available
If len(strFax) > 1 then
	objSelection.Font.Bold = True
	objSelection.Font.Size = 10 
	objSelection.TypeText " | Fax: "
	objSelection.Font.Bold = False
	objSelection.TypeText strFax
end if

'objSelection.TypeParagraph()

objSelection.ParagraphFormat.Alignment = 0
objSelection.TypeParagraph()
'#######  Mail and web links
objSelection.Font.Bold = True
objSelection.Font.Size = 10 
objSelection.TypeText "Email: "
Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "mailto: " & strEmail, , , strEmail) 
	objLink.Range.Font.Name = "arial" 
	objLink.Range.Font.Size = 10 
	objLink.Range.Font.Bold = false 

' ####### Add Skype user name if available
If Len(strSkype) > 1 then
	objSelection.Font.Bold = True
	objSelection.Font.Size = 10 
	objSelection.TypeText " | Skype: "
	objSelection.Font.Bold = False
	objSelection.TypeText strSkype
end if

' ####### Add Web Link
objSelection.Font.Bold = True
objSelection.TypeText " | Web: " 
Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "http://www.westchestergear.com", , , "www.westchestergear.com") 
	objLink.Range.Font.Name = "Arial" 
	objLink.Range.Font.Size = 10 
	objLink.Range.Font.Bold = false 
	objSelection.Font.Color = RGB (000,045,154) 

'	objSelection.TypeParagraph()
'	objSelection.TypeParagraph()
' #######  If the assistant field contains an user LDAP string get the data for that user to use on sig as well
If len(strAssistant) > 1 then
	objSelection.ParagraphFormat.Alignment = 0
	objSelection.TypeParagraph()
	Set objUserAssist = GetObject("LDAP://" & strAssistant)
		strAsGiven = objuserAssist.givenName 
		strAsSurname = objuserAssist.sn 
		strAsFax = objuserAssist.facsimileTelephoneNumber 
		strAsMobile = objuserAssist.mobile 
		strAsTitle = objUserAssist.Title 
		strAsDepartment = objUserAssist.Department 
		strAsEmail =objuserAssist.mail 
		strAsExt = objuserAssist.ipPhone 
		strAsSkype =objuserAssist.pager 
		strAsEmailTEXT = "Email: " 
	objSelection.Font.Name = "Arial" 
	objSelection.Font.Size = 8 
	objselection.Font.Bold = false 
	objSelection.Font.Color = RGB (000,000,000)
	objSelection.ParagraphFormat.Alignment = 0 
	objSelection.TypeParagraph()
	objSelection.Font.Bold = True 
	objSelection.TypeText strAsTitle & " | " & strAsGiven & " " & strAsSurname
	objSelection.ParagraphFormat.Alignment = 0
	objSelection.TypeParagraph()
	objSelection.Font.Bold = True
	objSelection.Font.Size = 8 
	objSelection.TypeText "Email: "
	Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "mailto: " & strAsEmail, , , strAsEmail) 
		objLink.Range.Font.Name = "arial" 
		objLink.Range.Font.Size = 8 
		objLink.Range.Font.Bold = false 
	objSelection.Font.Bold = True
	objSelection.Font.Size = 8 
	objSelection.TypeText " | Office: "
	objSelection.Font.Bold = False
	objSelection.TypeText "800.647.1900 ext. " & strAsExt
	If len(strFax) > 1 then
		objSelection.Font.Bold = True
		objSelection.Font.Size = 8
		objSelection.TypeText " | Fax: "
		objSelection.Font.Bold = False
		objSelection.TypeText strAsFax
	end if
	
End if

'######### Logo and media links
'objLink.Range.Font.Size = 6
'objLink.Range.Font.Bold = false
objSelection.TypeText Chr(11)
objSelection.ParagraphFormat.Alignment = 0 
objSelection.TypeParagraph() 
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\40WCPG_logo.png"), "http://www.westchestergear.com", , , "www.westchestergear.com") 
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\Email3.png"), "http://www.westchestergear.com", , , "www.westchestergear.com") 
	objLink.Range.Font.Name = "Arial" 
	objLink.Range.Font.Size = 10 
	objLink.Range.Font.Bold = false 
	objSelection.Font.Color = RGB (000,045,154) 

objselection.typetext chr(9)
REM ' ######### Social Media Links
REM objTable.Cell(2).select
set objlink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\FB-f-Logo__blue_29-2.png"), "https://www.facebook.com/pages/West-Chester-Protective-Gear/235840629898043", , , "Find us on Facebook")
objSelection.TypeText "     "
set objlink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\twitter-bird-light-bgs29.png"), "https://twitter.com/westchestergear", , , "Follow us on Twitter") 
objSelection.TypeText "     "
set objlink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\LinkedIn-29px-R.png"), "http://www.linkedin.com/company/west-chester-holdings", , , "") 
objSelection.TypeText "     "
set objlink = objselection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\yt_icon_rgb-29.png"), "https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube")
objSelection.TypeText "     "
set objlink = objselection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\glyph-logo_May2016-29.png"), "https://www.instagram.com/westchesterprotective/", , , "Follow us on Instagram")



objSelection.TypeParagraph()
set objlink = objselection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\HiViz.png"), "http://westchesterprotects.com/sites/default/files/catalog/2017_HiVis_Catalog_lr.pdf", , , "West Chester Protective Gear Hi-Viz Catalog")  
 
'objSelection.EndKey END_OF_STORY 

' ########### Tells outlook to use this signature for new messages and replys. Signature is called Email Signature. 
Set objSelection = objDoc.Range() 
objSignatureEntries.Add "WC_Corporate", objSelection 
objSignatureObject.NewMessageSignature = "WC_Corporate" 
objSignatureObject.ReplyMessageSignature = "WC_Corporate" 
 
objDoc.Saved = True 
objWord.Quit