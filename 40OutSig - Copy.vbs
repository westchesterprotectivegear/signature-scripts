'========================== 
' 
' ############### Script that creates a standard company signature for all users (in a corporate environment); pulling data from Active Directory - dynamic logo insertion depending on department field if needed. 
' 
' Original by Severn Dickinson 
'
' Big up to Script Centre 
' 
' 17-01-2011. 
' 
'=========================== 
 
On Error Resume Next 
Set objSysInfo = CreateObject("ADSystemInfo") 
 
 
' ########### This section connects to Active Directory as the currently logged on user 
strUser = objSysInfo.UserName 
Set objUser = GetObject("LDAP://" & strUser)  
 
 
' ########### This section sets up the variables we want to call in the script (items on the left; whereas the items on the right are the active directory database field names) - ie strVariablename = objuser.ad.databasename 
strGiven = UCase(objuser.givenName)
If Len(objuser.initials) = 1 then
	strInitials = UCase(objuser.initials) & ". "
end if
strSurname = UCase(objuser.sn)
strAddress1 = objUser.streetaddress 
strAddress1EXT = objUser.postofficebox 
strCity = objuser.l 
strRegion = objuser.st 
strPostcode = objuser.postalcode 
strCountry = objuser.c 
strFax = objuser.facsimileTelephoneNumber 
strMobile = objuser.mobile 
strTitle = objUser.Title 
strDepartment = objUser.Department 
strCompany = objUser.Company 
strPhone = objUser.telephoneNumber 
strEmail =objuser.mail 
strWeb = objuser.wWWHomePage 
strNotes = objuser.info 
strExt = objuser.ipPhone 
strDDI = objuser.homephone
strSkype =objuser.pager 
strAssistant = objUser.Assistant
strEmailTEXT = "Email: " 
strAccName = objUser.sAMAccountName
strPostNominal = UCase(objUser.extensionAttribute1)

' ########### Map Network Drives to PC
Dim objNetwork, strLocalDrive, strRemoteShare
Set objNetwork = WScript.CreateObject("WScript.Network")
strLocalDrive = "H:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Users\" & sAMAccountName
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

strLocalDrive = "P:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Public"
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

Set colGroups = objuser.groups
For Each objGroup in colGroups
	If objGroup.cn = "KAM" Then
		strLocalDrive = "G:"
		strRemoteShare = "\\wcmktg.com\WCPG HQ\Design"
		'objNetwork.RemoveNetworkDrive strLocalDrive
		objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True
	End if
Next		
 
 
' ########### Sets up word template 
Set objWord = CreateObject("Word.Application") 
Set objDoc = objWord.Documents.Add() 
Set objSelection = objWord.Selection 
objSelection.Style = "No Spacing" 
Set objEmailOptions = objWord.EmailOptions 
Set objSignatureObject = objEmailOptions.EmailSignature 
Set objSignatureEntries = objSignatureObject.EmailSignatureEntries 
 
 
' ########### Calls the variables from above section and inserts into word template, also sets initial font typeface, colour etc. 
objSelection.Font.Name = "Arial" 
objSelection.Font.Size = 11 
objselection.Font.Bold = false 
objSelection.Font.Color = RGB (000,000,000) 

'test new table sig
Const RowN = 9
Const ColN = 9
Const END_OF_STORY=6
objSelection.TypeParagraph()

Set objRange =objSelection.Range
objDoc.Tables.Add objRange, RowN, ColN

Set objTable = objDoc.Tables(1)
'Merge Cells to create format
objTable.Cell(1, 1).Merge objTable.Cell(1, 9)
objTable.Cell(2, 3).Merge objTable.Cell(2, 9)
objTable.Cell(3, 3).Merge objTable.Cell(3, 9)
objTable.Cell(4, 3).Merge objTable.Cell(4, 9)
objTable.Cell(5, 3).Merge objTable.Cell(5, 9)
objTable.Cell(6, 3).Merge objTable.Cell(6, 9)
objTable.Cell(2, 1).Merge objTable.Cell(6, 2)
objTable.Cell(7, 1).Merge objTable.Cell(7, 9)
objTable.Cell(8, 1).Merge objTable.Cell(8, 4)
objTable.Cell(8, 4).Merge objTable.Cell(8, 6)
objTable.Cell(9, 1).Merge objTable.Cell(9, 4)

Objtable.Borders.Enable=False

'Signature Table now setup.  Time to enter data.
objTable.Cell(2,1).Select
ObjTable.Cell(2,1).BottomPadding = 0
objTable.Cell(2,1).LeftPadding = 0
objTable.Cell(2,1).RightPadding= 0
objTable.Cell(2,1).TopPadding = 0
objTable.Cell(2,1).ParagraphFormat.Alignment = wdAlignParagraphCenter
objTable.Cell(2,1).Cells.VerticalAlignment = wdCellAlignVerticalCenter
With objTable.Cell(2,1)
	.Borders(wdBorderTop).LineStyle = wdLineStyleNone
	.Borders(wdBorderLeft).LineStyle = wdLineStyleNone
    .Borders(wdBorderBottom).LineStyle = wdLineStyleNone
    .Borders(wdBorderRight).LineStyle = wdLineStyleSingle
    .Borders(wdBorderDiagonalDown).LineStyle = wdLineStyleNone
    .Borders(wdBorderDiagonalUp).LineStyle = wdLineStyleNone
End With


Set objShape = objTable.Cell(2,1).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\WCPG_40_Logo_Color96.png")
'objShape.Height = 80
'objShape.Width = 80
objdoc.Hyperlinks.Add objShape,"http://www.westchestergear.com", , , "www.westchestergear.com"

'blank buffer space
objTable.Cell(1,1).Select
objTable.Cell(1,1).Borders.Enable = False
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6
objselection.Font.Bold = false

'Employee Name and post nomial
objTable.Cell(2,2).Select
'bjTable.Cell(2,2).Borders.Enable = False
With objTable.Cell(2,2)
.Borders(wdBorderTop).LineStyle = wdLineStyleNone
	.Borders(wdBorderLeft).LineStyle = wdLineStyleSingle
    .Borders(wdBorderBottom).LineStyle = wdLineStyleNone
    .Borders(wdBorderRight).LineStyle = wdLineStyleNone
    .Borders(wdBorderDiagonalDown).LineStyle = wdLineStyleNone
    .Borders(wdBorderDiagonalUp).LineStyle = wdLineStyleNone
    .Borders(wdBorderLeft).LineWidth = wdLineWidth225pt
    .Borders(wdBorderLeft).Color = RGB (000,000,000)
    
End With

objSelection.Font.Name ="Arial"
objSelection.Font.Size = 20 
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.Text = strGiven & " " & strinitials & strSurname & strPostNominal

'Employee Title
objTable.Cell(3,2).Select
objTable.Cell(3,2).Borders.Enable = False
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 13 
objselection.Font.Bold = True
objselection.Font.Italic = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.Text = strTitle

'Work 800 number and extension
objTable.Cell(4,2).Select
objTable.Cell(4,2).Borders.Enable = False
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 11
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.TypeText "800.647.1900 ext. " & strExt

'Add Direct Dial
objTable.Cell(5,2).Select
objTable.Cell(5,2).Borders.Enable = False
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 11 
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.TypeText "Direct: " & strPhone

'Add Web address
objTable.Cell(6,2).Select
objTable.Cell(6,2).Borders.Enable = False
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 11
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.TypeText "Web: " 
Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "http://www.westchestergear.com", , , "www.westchestergear.com") 
objLink.Range.Font.Name = "Arial" 
objLink.Range.Font.Size = 11 
objLink.Range.Font.Bold = False
objSelection.Font.Color = RGB (000,045,154) 

'blank buffer space
objTable.Cell(7,1).Select
objTable.Cell(7,1).Borders.Enable = False
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = false

'Icon Row for social icons
'objTable.Rows(8).HeightRule = wdRowHeightAtLeast
'objTable.Rows(8).Height = 40
objTable.Cell(8,1).Select
objTable.Cell(8,1).Borders.Enable = True
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
objSelection.TypeParagraph()
'objSelection.Shading.Texture = wdTexture100Percent
'objSelection.Shading.BackgroundPatternColor = wdBlue
objTable.Cells(8,1).Range.ParagraphFormat.Alignment = wdAlignParagraphCenter
objTable.Cells(8,1).Range.VerticalAlignment = wdCellAlignVerticalCenter
objTable.Cells(8,1).Range.text = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\VizUp96.png"), "http://www.westchesterprotects.com/stand-out-on-the-job-with-hi-viz-apparel/", , , "Viz Up with West Chester Protective Gear") 
'Set objShape = objTable.Cell(8,1).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\VizUp96.png")
'objShape.Height = 50
'objShape.Width = 411
'objdoc.Hyperlinks.Add objShape,"http://www.westchestergear.com", , , "www.westchestergear.com"
objSelection.TypeParagraph()


'Facebook Social
objTable.Cell(8,2).Select
objTable.Cell(8,2).Borders.Enable = True
objTable.Cell(8,2).SetWidth = InchesToPoints(0.3)
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
objSelection.TypeParagraph()
'objSelection.Shading.Texture = wdTexture100Percent
'objSelection.Shading.BackgroundPatternColor = wdBlack
objSelection.ParagraphFormat.Alignment = wdAlignParagraphCenter
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter
'Set objShape = objTable.Cell(8,2).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Facebook96.png")
'objdoc.Hyperlinks.Add objShape,  "https://www.facebook.com/pages/West-Chester-Protective-Gear/235840629898043", , , "Find us on Facebook"
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Facebook96.png"),  "https://www.facebook.com/pages/West-Chester-Protective-Gear/235840629898043", , , "Find us on Facebook")
objSelection.TypeParagraph()

'Twitter Social
objTable.Cell(8,3).Select
objTable.Cell(8,3).Borders.Enable = True
objTable.Cell(8,3).SetWidth = InchesToPoints(0.3)
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
objSelection.TypeParagraph()
'objSelection.Shading.Texture = wdTexture100Percent
'objSelection.Shading.BackgroundPatternColor = wdBlack
objSelection.ParagraphFormat.Alignment = wdAlignParagraphCenter
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter
'Set objShape = objTable.Cell(8,3).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Twitter96.png")
'objdoc.Hyperlinks.Add objShape,  "https://twitter.com/westchestergear", , , "Follow us on Twitter" 
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Twitter96.png"), "https://twitter.com/westchestergear", , , "Follow us on Twitter") 
objSelection.TypeParagraph()

'Social Glyph bar
objTable.Cell(8,4).Select
objTable.Cell(8,4).Borders.Enable = True
objTable.Cell(8,4).SetWidth = InchesToPoints(0.3)
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
objSelection.TypeParagraph()
objSelection.Shading.Texture = wdTexture100Percent
objSelection.Shading.BackgroundPatternColor = wdBlack
objSelection.ParagraphFormat.Alignment = wdAlignParagraphCenter
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter
'Set objShape = objTable.Cell(8,4).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Instagram96.png")
'objdoc.Hyperlinks.Add objShape, "https://www.instagram.com/westchesterprotective/", , , "Follow us on Instagram"
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Facebook96.png"),  "https://www.facebook.com/pages/West-Chester-Protective-Gear/235840629898043", , , "Find us on Facebook")
objSelection.TypeText "  "
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Twitter96.png"), "https://twitter.com/westchestergear", , , "Follow us on Twitter") 
objSelection.TypeText "  "
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Instagram96.png"), "https://www.instagram.com/westchesterprotective/", , , "Follow us on Instagram")
objSelection.TypeText "  "
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\LinkedIn96.png"), "http://www.linkedin.com/company/west-chester-holdings", , , "") 
objSelection.TypeText "  "
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Youtube96.png"), "https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube")
objSelection.TypeParagraph()

'LinkedIn Social
objTable.Cell(8,5).Select
objTable.Cell(8,5).Borders.Enable = True
objTable.Cell(8,5).SetWidth = InchesToPoints(0.3)
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
objSelection.TypeParagraph()
'objSelection.Shading.Texture = wdTexture100Percent
'objSelection.Shading.BackgroundPatternColor = wdBlack
objSelection.ParagraphFormat.Alignment = wdAlignParagraphCenter
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter
'Set objShape = objTable.Cell(8,5).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\LinkedIn96.png")
'objdoc.Hyperlinks.Add objShape, "http://www.linkedin.com/company/west-chester-holdings", , , ""
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\LinkedIn96.png"), "http://www.linkedin.com/company/west-chester-holdings", , , "") 
objSelection.TypeParagraph()

'YouTube social
objTable.Cell(8,6).Select
objTable.Cell(8,6).Borders.Enable = True
objTable.Cell(8,6).SetWidth = InchesToPoints(0.3)
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
objSelection.TypeParagraph()
'objSelection.Shading.Texture = wdTexture100Percent
'objSelection.Shading.BackgroundPatternColor = wdBlack
objSelection.ParagraphFormat.Alignment = wdAlignParagraphCenter
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter
'Set objShape = objTable.Cell(8,6).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Youtube96.png")
'objdoc.Hyperlinks.Add objShape,  "https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube"
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Youtube96.png"), "https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube")
objSelection.TypeParagraph()

'Call Me Tag
objTable.Cell(9,1).Select
objTable.Cell(9,1).Borders.Enable = False
ObjTable.Cell(9,1).BottomPadding = 0
objTable.Cell(9,1).LeftPadding = 0
objTable.Cell(9,1).RightPadding= 0
objTable.Cell(9,1).TopPadding = 0
objTable.Cell(9,1).Borders(wdBorderTop).LineStyle = wdLineStyleSingle
	objTable.Cell(9,1).Borders(wdBorderLeft).LineStyle = wdLineStyleNone
    objTable.Cell(9,1).Borders(wdBorderBottom).LineStyle = wdLineStyleNone
    objTable.Cell(9,1).Borders(wdBorderRight).LineStyle = wdLineStyleNone
    objTable.Cell(9,1).Borders(wdBorderDiagonalDown).LineStyle = wdLineStyleNone
    objTable.Cell(9,1).Borders(wdBorderDiagonalUp).LineStyle = wdLineStyleNone
    objTable.Cell(9,1).Borders(wdBorderLeft).LineWidth = wdLineWidth225pt
    objTable.Cell(9,1).Borders(wdBorderLeft).Color = RGB (000,000,000)
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
'objSelection.TypeParagraph()
'objSelection.Shading.Texture = wdTexture100Percent
'objSelection.Shading.BackgroundPatternColor = wdBlack
objSelection.ParagraphFormat.Alignment = wdAlignParagraphCenter
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter
'Set objShape = objTable.Cell(8,6).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Youtube96.png")
'objdoc.Hyperlinks.Add objShape,  "https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube"
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\CallMe96.png"), "http://www.westchesterprotects.com/stand-out-on-the-job-with-hi-viz-apparel/", , , "Customize your Hi-Viz apparel here at West Chester Protective Gear")
'objSelection.TypeParagraph()


objTable.Select
objSelection.EndKey 6

If len(strAssistant) > 1 then
	objSelection.ParagraphFormat.Alignment = 0
	Set objUserAssist = GetObject("LDAP://" & strAssistant)
		strAsGiven = objuserAssist.givenName 
		strAsSurname = objuserAssist.sn 
		strAsFax = objuserAssist.facsimileTelephoneNumber 
		strAsMobile = objuserAssist.mobile 
		strAsTitle = objUserAssist.Title 
		strAsDepartment = objUserAssist.Department 
		strAsEmail =objuserAssist.mail 
		strAsExt = objuserAssist.ipPhone 
		strAsSkype =objuserAssist.pager 
		strAsEmailTEXT = "Email: " 
	objSelection.Font.Name = "Arial" 
	objSelection.Font.Size = 9 
	objselection.Font.Bold = True 
	objSelection.Font.Color = RGB (000,000,000)
	objSelection.ParagraphFormat.Alignment = 0 
	objSelection.TypeParagraph()
	objSelection.Font.Bold = True 
	objSelection.TypeText strAsTitle & ": " & strAsGiven & " " & strAsSurname
	objSelection.ParagraphFormat.Alignment = 0
	objSelection.TypeParagraph()
	objSelection.Font.Bold = True
	objSelection.Font.Size = 9 
	objSelection.TypeText "Email: "
	Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "mailto: " & strAsEmail, , , strAsEmail) 
		objLink.Range.Font.Name = "arial" 
		objLink.Range.Font.Size = 9 
		objLink.Range.Font.Bold = False
	objSelection.TypeParagraph() 
	objSelection.Font.Bold = True
	objSelection.Font.Size = 9 
	objSelection.TypeText "Office: "
	objSelection.Font.Bold = True
	objSelection.TypeText "800.647.1900 ext. " & strAsExt
		
End if

'Set objTable = Nothing
'set objSelection = objDoc.Select
'objSelection.MoveDown(Unit:=wdParagraph, Count:=1, Extend:=wdMove)
'objSelection.TypeParagraph()
'objDoc.Range.InsertAfter


' ########### Tells outlook to use this signature for new messages and replys. Signature is called Email Signature. 
Set objSelection = objDoc.Range() 
objSignatureEntries.Add "WC_Corporate", objSelection 
objSignatureObject.NewMessageSignature = "WC_Corporate" 
objSignatureObject.ReplyMessageSignature = "WC_Corporate" 
 
objDoc.Saved = True 
objWord.Quit