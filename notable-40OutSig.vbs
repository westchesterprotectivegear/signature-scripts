'========================== 
' 
' ############### Script that creates a standard company signature for all users (in a corporate environment); pulling data from Active Directory - dynamic logo insertion depending on department field if needed. 
' 
' Original by Severn Dickinson 
'
' Big up to Script Centre 
' 
' 17-01-2011. 
' 
'=========================== 
 
On Error Resume Next 
Set objSysInfo = CreateObject("ADSystemInfo") 
 
' ########### This section connects to Active Directory as the currently logged on user 
strUser = objSysInfo.UserName 
Set objUser = GetObject("LDAP://" & strUser)  
 
' ########### This section sets up the variables we want to call in the script (items on the left; whereas the items on the right are the active directory database field names) - ie strVariablename = objuser.ad.databasename 
strGiven = UCase(objuser.givenName)
If Len(objuser.initials) = 1 then
	strInitials = UCase(objuser.initials) & ". "
end if
strSurname = UCase(objuser.sn)
strAddress1 = objUser.streetaddress 
strAddress1EXT = objUser.postofficebox 
strCity = objuser.l 
strRegion = objuser.st 
strPostcode = objuser.postalcode 
strCountry = objuser.c 
strFax = objuser.facsimileTelephoneNumber 
strMobile = objuser.mobile 
strTitle = objUser.Title 
strDepartment = objUser.Department 
strCompany = objUser.Company 
strPhone = objUser.telephoneNumber 
strEmail =objuser.mail 
strWeb = objuser.wWWHomePage 
strNotes = objuser.info 
strExt = objuser.ipPhone 
strDDI = objuser.homephone
strSkype =objuser.pager 
strAssistant = objUser.Assistant
strEmailTEXT = "Email: " 
strAccName = objUser.sAMAccountName
strPostNominal = UCase(objUser.extensionAttribute1)

' ########### Map Network Drives to PC
Dim objNetwork, strLocalDrive, strRemoteShare
Set objNetwork = WScript.CreateObject("WScript.Network")
strLocalDrive = "H:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Users\" & sAMAccountName
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

strLocalDrive = "P:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Public"
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

Set colGroups = objuser.groups
For Each objGroup in colGroups
	If objGroup.cn = "KAM" Then
		strLocalDrive = "G:"
		strRemoteShare = "\\wcmktg.com\WCPG HQ\Design"
		'objNetwork.RemoveNetworkDrive strLocalDrive
		objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True
	End if
Next		

' ########### Sets up word template 
Set objWord = CreateObject("Word.Application") 
Set objDoc = objWord.Documents.Add() 
Set objSelection = objWord.Selection 
objSelection.Style = "No Spacing" 
Set objEmailOptions = objWord.EmailOptions 
Set objSignatureObject = objEmailOptions.EmailSignature 
Set objSignatureEntries = objSignatureObject.EmailSignatureEntries 

  With ActiveDocument.Styles("Hyperlink").Font
    .Name = "Arial"
    .Underline = wdUnderlineSingle
    .Color = wdBlack
  End With

With objDoc.Styles("Paragraph").Font
	.Name = "Arial"
	.Color = wdBlack
	.Bold = True 
End With
  
Dim objShape
  
'Set objShape = objTable.Cell(1,1).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\WCPG_40_Logo_Color962.png")
'objdoc.Hyperlinks.Add objShape,"http://www.westchestergear.com", , , "www.westchestergear.com" 
objselection.TypeParagraph()
Set objShape = objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\WCPG_40_Logo_Color962.png",False,True).ConvertToShape
'objshape.ConvertToShape = True
objShape.WrapFormat.Type = 0
objdoc.Hyperlinks.Add objShape,"http://www.westchestergear.com", , , "www.westchestergear.com"

UserInfoCnt = 0

'##### Employee Name
objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5)
objSelection.Font.Name ="Arial"
objSelection.Font.Size =  12
objselection.Font.Bold = True 
objSelection.TypeText strGiven & " " & strinitials & strSurname & strPostNominal 

'##### Employee title  
objselection.TypeParagraph()
objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5)
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 11
objselection.Font.Bold = True 
objselection.Font.Italic = True 
objSelection.TypeText strTitle 

'##### 800 Number with Extension  
objselection.TypeParagraph()
objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5) 
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 9
objselection.Font.Bold = True 
objselection.Font.Italic = False
objSelection.TypeText "Office: 800.647.1900"
'##### See if user has an extension
If Len(strExt) > 1 Then 
	objSelection.TypeText " ext. " & strExt
End If
UserInfoCnt = UserInfoCnt + 1

'##### Direct Dial number
If Len(strPhone) > 1 Then
	objselection.TypeParagraph()
	objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5) 
	objSelection.Font.Name ="Arial"
	objSelection.Font.Size = 9
	objselection.Font.Bold = True 
	objselection.Font.Italic = False
	objSelection.TypeText "Direct: " & strPhone
	UserInfoCnt = UserInfoCnt + 1
End if

'##### Fax Number
If Len(strFax) >1 Then
	objselection.TypeParagraph()
	objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5) 
	objSelection.Font.Name ="Arial"
	objSelection.Font.Size = 9
	objselection.Font.Bold = True 
	objselection.Font.Italic = False
	objSelection.TypeText "Fax: " & strFax
	UserInfoCnt = UserInfoCnt + 1
End If

'##### Location and Address via google maps
objselection.TypeParagraph()
objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5) 
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 9
objselection.Font.Bold = True 
objselection.Font.Italic = False
objSelection.TypeText "Location: " 
Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "https://www.google.com/maps/place/West+Chester+Protective+Gear/@39.280435,-84.4379556,318m/data=!3m1!1e3!4m13!1m7!3m6!1s0x884051e2b2fbb409:0xda336d45c6e1d66b!2s11500+Canal+Rd,+Sharonville,+OH+45241!3b1!8m2!3d39.2814883!4d-84.4374814!3m4!1s0x88405ecbce1059ab:0x1d5bfd72bef04af2!8m2!3d39.2807776!4d-84.4369376", , , "Google Maps") 
objLink.Range.Font.Name = "Arial" 
objLink.Range.Font.Size = 9 
objLink.Range.Font.Bold = False
objSelection.Font.Color = RGB (000,045,154)
UserInfoCnt = UserInfoCnt + 1

'##### Web address
objselection.TypeParagraph()
objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5) 
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 9
objselection.Font.Bold = True 
objselection.Font.Italic = False
objSelection.Font.Color = RGB (000,000,000)
objSelection.TypeText "Web: " 
Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "http://www.westchestergear.com", , , "www.westchestergear.com") 
objLink.Range.Font.Name = "Arial" 
objLink.Range.Font.Size = 9 
objLink.Range.Font.Bold = False
objSelection.Font.Color = RGB (000,045,154)
UserInfoCnt = UserInfoCnt + 1

'##### Fill in Blank spaces if users are missing fields
If UserInfoCnt < 5 Then
	Do While UserInfoCnt < 5
		objselection.TypeParagraph()
		objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5) 
		objSelection.Font.Name ="Arial"
		objSelection.Font.Size = 9
		objselection.Font.Bold = True 
		objselection.Font.Italic = False
		strvalue = objSelection.Shading.BackgroundPatternColorIndex
		'## debug purpose
		'objSelection.TypeText "hello" & objDoc.Paragraphs.Count
		UserInfoCnt = UserInfoCnt + 1
	Loop
End If

		objselection.TypeParagraph()
		objSelection.ParagraphFormat.LineSpacing = LinesToPoints(1.5) 
		objSelection.Font.Name ="Arial"
		objSelection.Font.Size = 6
		objselection.Font.Bold = True 
		objselection.Font.Italic = False

'##### Graphic bar
objSelection.TypeParagraph()
objSelection.Font.Size = 2
objSelection.TypeParagraph
'##### For tracking of what paragraphs to add black shading for bottom bar.
ParShadeStart = objDoc.Paragraphs.Count
objSelection.TypeParagraph()
objSelection.Font.Size = 12
objSelection.ParagraphFormat.Alignment.wdAlignParagraphRight
Set objShape = objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\VizUp96.png",False,True).ConvertToShape
objShape.WrapFormat.Type = 6
objShape.Left = 15

'objSelection.TypeParagraph()
'objSelection.Font.Size = 18
'objSelection.Paragraphs.TabStops.Add inchestopoints(2), 2
objSelection.ParagraphFormat.Alignment = 2
Set objShape = objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Facebook96.png")
objdoc.Hyperlinks.Add objShape,"https://www.facebook.com/pages/West-Chester-Protective-Gear/235840629898043", , , "Find us on Facebook"
Set objShape = objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Twitter96.png")
objdoc.Hyperlinks.Add objShape,"https://twitter.com/westchestergear", , , "Follow us on Twitter"
Set objShape = objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Instagram96.png")
objdoc.Hyperlinks.Add objShape,"https://www.instagram.com/westchesterprotective/", , , "Follow us on Instagram"
Set objShape = objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\LinkedIn96.png")
objdoc.Hyperlinks.Add objShape,"http://www.linkedin.com/company/west-chester-holdings", , , ""
Set objShape = objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Youtube96.png")
objdoc.Hyperlinks.Add objShape,"https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube"
objSelection.TypeParagraph()
objSelection.Font.Size = 2

If len(strAssistant) > 1 Then
	'objSelection.Shading.Texture = wdTextureNone
	'objSelection.Shading.ForegroundPatternColor = wdColorWhite
	'objSelection.Shading.BackgroundPatternColorIndex.wdWhite
	objSelection.ParagrphFormat.Alignment = 0
	Set objUserAssist = GetObject("LDAP://" & strAssistant)
		strAsGiven = objuserAssist.givenName 
		strAsSurname = objuserAssist.sn 
		strAsFax = objuserAssist.facsimileTelephoneNumber 
		strAsMobile = objuserAssist.mobile 
		strAsTitle = objUserAssist.Title 
		strAsDepartment = objUserAssist.Department 
		strAsEmail =objuserAssist.mail 
		strAsExt = objuserAssist.ipPhone 
		strAsSkype =objuserAssist.pager 
		strAsEmailTEXT = "Email: " 
		objSelection.TypeParagraph()
		objSelection.Font.Name = "Arial" 
		objSelection.Font.Size = 9 
		objselection.Font.Bold = True 
		objSelection.Font.Color = RGB (000,000,000)
		objSelection.ParagraphFormat.Alignment = 0
		objSelection.Font.Bold = True 
		objSelection.TypeText strAsTitle & ": " & strAsGiven & " " & strAsSurname
		objSelection.ParagraphFormat.Alignment = 0
		objSelection.TypeParagraph()
		objSelection.Font.Bold = True
		objSelection.Font.Size = 9 
		objSelection.TypeText "Email: "
		Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "mailto: " & strAsEmail, , , strAsEmail) 
		objLink.Range.Font.Name = "Arial" 
		objLink.Range.Font.Size = 9 
		objLink.Range.Font.Bold = False
		objSelection.TypeParagraph() 
		objSelection.Font.Bold = True
		objSelection.Font.Size = 9 
		objSelection.TypeText "Office: "
		objSelection.Font.Bold = True
		objSelection.TypeText "800.647.1900 ext. " & strAsExt		
End If

ParShadeEnd = ParShadeStart + 3
Do While ParShadeStart < ParShadeEnd
	Set myRange = objDoc.Paragraphs(ParShadeStart).Range
		myRange.Shading.Texture = wdTextureNone
		myRange.Shading.ForegroundPatternColor = wdColorAutomatic
		myRange.Shading.BackgroundPatternColor = -587137025
		ParShadeStart = ParShadeStart +1
Loop  
  
' ########### Tells outlook to use this signature for new messages and replys. Signature is called Email Signature. 
Set objSelection = objDoc.Range() 
objSignatureEntries.Add "WCPG_40th_Corporate", objSelection 
objSignatureObject.NewMessageSignature = "WCPG_40th_Corporate" 
objSignatureObject.ReplyMessageSignature = "WCPG_40th_Corporate" 
 
objDoc.Saved = True 
objWord.Quit