'========================== 
' 
' ############### Script that creates a standard company signature for all users (in a corporate environment); pulling data from Active Directory - dynamic logo insertion depending on department field if needed. 
' 
' Original by Severn Dickinson 
'
' Big up to Script Centre 
' 
' 17-01-2011. 
' 
'=========================== 
 
On Error Resume Next 
Set objSysInfo = CreateObject("ADSystemInfo") 
 
' ########### This section connects to Active Directory as the currently logged on user 
strUser = objSysInfo.UserName 
Set objUser = GetObject("LDAP://" & strUser)  
 
' ########### This section sets up the variables we want to call in the script (items on the left; whereas the items on the right are the active directory database field names) - ie strVariablename = objuser.ad.databasename 
strGiven = UCase(objuser.givenName)
If Len(objuser.initials) = 1 then
	strInitials = UCase(objuser.initials) & ". "
end if
strSurname = UCase(objuser.sn)
strAddress1 = objUser.streetaddress 
strAddress1EXT = objUser.postofficebox 
strCity = objuser.l 
strRegion = objuser.st 
strPostcode = objuser.postalcode 
strCountry = objuser.c 
strFax = objuser.facsimileTelephoneNumber 
strMobile = objuser.mobile 
strTitle = objUser.Title 
strDepartment = objUser.Department 
strCompany = objUser.Company 
strPhone = objUser.telephoneNumber 
strEmail =objuser.mail 
strWeb = objuser.wWWHomePage 
strNotes = objuser.info 
strExt = objuser.ipPhone 
strDDI = objuser.homephone
strSkype =objuser.pager 
strAssistant = objUser.Assistant
strEmailTEXT = "Email: " 
strAccName = objUser.sAMAccountName
strPostNominal = UCase(objUser.extensionAttribute1)

' ########### Map Network Drives to PC
Dim objNetwork, strLocalDrive, strRemoteShare
Set objNetwork = WScript.CreateObject("WScript.Network")
strLocalDrive = "H:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Users\" & sAMAccountName
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

strLocalDrive = "P:"
strRemoteShare = "\\wcmktg.com\WCPG HQ\Public"
'objNetwork.RemoveNetworkDrive strLocalDrive
objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True

Set colGroups = objuser.groups
For Each objGroup in colGroups
	If objGroup.cn = "KAM" Then
		strLocalDrive = "G:"
		strRemoteShare = "\\wcmktg.com\WCPG HQ\Design"
		'objNetwork.RemoveNetworkDrive strLocalDrive
		objNetwork.MapNetworkDrive strLocalDrive, strRemoteShare, True
	End if
Next		
  
' ########### Sets up word template 
Set objWord = CreateObject("Word.Application") 
Set objDoc = objWord.Documents.Add() 
Set objSelection = objWord.Selection 
objSelection.Style = "No Spacing" 
Set objEmailOptions = objWord.EmailOptions 
Set objSignatureObject = objEmailOptions.EmailSignature 
Set objSignatureEntries = objSignatureObject.EmailSignatureEntries 

  With ActiveDocument.Styles("Hyperlink").Font
    .Name = ""
    .Underline = wdUnderlineSingle
    .Color = wdBlack
  End With
 
 
' ########### Calls the variables from above section and inserts into word template, also sets initial font typeface, colour etc. 
objSelection.Font.Name = "Arial" 
objSelection.Font.Size = 11 
objselection.Font.Bold = false 
objSelection.Font.Color = RGB (000,000,000) 

'test new table sig
Const RowN = 7
Const ColN = 9
Const END_OF_STORY=6
objSelection.TypeParagraph()

Set objRange =objSelection.Range
objDoc.Tables.Add objRange, RowN, ColN

Set objTable = objDoc.Tables(1)
'Merge Cells to create format
objTable.Cell(1, 3).Merge objTable.Cell(1, 9)
objTable.Cell(2, 3).Merge objTable.Cell(2, 9)
objTable.Cell(3, 3).Merge objTable.Cell(3, 9)
objTable.Cell(4, 3).Merge objTable.Cell(4, 9)
objTable.Cell(5, 3).Merge objTable.Cell(5, 9)
objTable.Cell(1, 1).Merge objTable.Cell(5, 2)
objTable.Cell(6, 1).Merge objTable.Cell(6, 4)
objTable.Cell(6, 2).Merge objTable.Cell(6, 6)
objTable.Cell(7, 1).Merge objTable.Cell(7, 4)

Objtable.Borders.Enable=False

'Signature Table now setup.  Time to enter data.
objTable.Cell(1,1).Select
ObjTable.Cell(1,1).BottomPadding = 0
objTable.Cell(1,1).LeftPadding = 0
objTable.Cell(1,1).RightPadding= 0
objTable.Cell(1,1).TopPadding = 0
objTable.Cell(1,1).ParagraphFormat.Alignment = wdAlignParagraphCenter
objTable.Cell(1,1).Cells.VerticalAlignment = wdCellAlignVerticalCenter
With objTable.Cell(1,1)
	.Borders(wdBorderTop).LineStyle = wdLineStyleNone
	.Borders(wdBorderLeft).LineStyle = wdLineStyleNone
    .Borders(wdBorderBottom).LineStyle = wdLineStyleNone
    .Borders(wdBorderRight).LineStyle = wdLineStyleSingle
    .Borders(wdBorderDiagonalDown).LineStyle = wdLineStyleNone
    .Borders(wdBorderDiagonalUp).LineStyle = wdLineStyleNone
End With


Set objShape = objTable.Cell(1,1).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\WCPG_40_Logo_Color962.png")
objdoc.Hyperlinks.Add objShape,"http://www.westchestergear.com", , , "www.westchestergear.com"

'Employee Name and post nomial
objTable.Cell(1,2).Select
objTable.Cell(1,2).TopPadding = 5
ObjTable.Cell(1,2).BottomPadding = 0
'bjTable.Cell(1,2).Borders.Enable = False
With objTable.Cell(1,2)
.Borders(wdBorderTop).LineStyle = wdLineStyleNone
	.Borders(wdBorderLeft).LineStyle = wdLineStyleSingle
    .Borders(wdBorderBottom).LineStyle = wdLineStyleNone
    .Borders(wdBorderRight).LineStyle = wdLineStyleNone
    .Borders(wdBorderDiagonalDown).LineStyle = wdLineStyleNone
    .Borders(wdBorderDiagonalUp).LineStyle = wdLineStyleNone
    .Borders(wdBorderLeft).LineWidth = wdLineWidth225pt
    .Borders(wdBorderLeft).Color = RGB (000,000,000)
    
End With
objTable.Cell(1,2).LeftPadding = 5
objSelection.Font.Name ="Arial"
objSelection.Font.Size =  16
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.Text = strGiven & " " & strinitials & strSurname & strPostNominal

'Employee Title
objTable.Cell(2,2).Select
objTable.Cell(2,2).Borders.Enable = False
objTable.Cell(2,2).LeftPadding = 5
ObjTable.Cell(2,2).TopPadding = 2
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 13 
objselection.Font.Bold = True
objselection.Font.Italic = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.Text = strTitle

'Work 800 number and extension
objTable.Cell(3,2).Select
objTable.Cell(3,2).Borders.Enable = False
objTable.Cell(3,2).LeftPadding = 5
objTable.Cell(3,2).TopPadding = 3
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 11
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.TypeText "Office: 800.647.1900 ext. " & strExt

'Add Direct Dial
objTable.Cell(4,2).Select
objTable.Cell(4,2).Borders.Enable = False
objTable.Cell(4,2).LeftPadding = 5
objTable.Cell(4,2).TopPadding = 3
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 11
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.TypeText "Direct: " & strPhone

'Add Web address
objTable.Cell(5,2).Select
objTable.Cell(5,2).Borders.Enable = False
objTable.Cell(5,2).LeftPadding = 5
objTable.Cell(5,2).TopPadding = 3
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 11
objselection.Font.Bold = True 
objSelection.Font.Color = RGB (000,000,000) 
objSelection.TypeText "Web: " 
Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "http://www.westchestergear.com", , , "www.westchestergear.com") 
objLink.Range.Font.Name = "Arial" 
objLink.Range.Font.Size = 10 
objLink.Range.Font.Bold = False
objSelection.Font.Color = RGB (000,045,154) 

'Icon Row for social icons
objTable.Cell(6,1).Select
objTable.Cell(6,1).Borders.Enable = True
objTable.Cell(6,1).TopPadding = 5
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
'objSelection.TypeParagraph()
objSelection.Shading.Texture = wdTexture100Percent
objSelection.Shading.BackgroundPatternColor = wdBlack
objTable.Cells(6,1).Range.ParagraphFormat.Alignment = wdAlignParagraphCenter
objTable.Cells(6,1).Range.VerticalAlignment = wdCellAlignVerticalCenter
objTable.Cells(6,1).Range.text = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\VizUp96.png"), "http://www.westchesterprotects.com/stand-out-on-the-job-with-hi-viz-apparel/", , , "Viz Up with West Chester Protective Gear") 
objTable.Cell(6,1).BottomPadding = 5
'objSelection.TypeParagraph()

'Social Glyph bar
objTable.Cell(6,2).Select
Set myRange = ActiveDocument.Range(Tables(1).Cell(6,2).Range.Start, Tables(1).Cell(6,2).Range.End)
objTable.Cell(6,2).TopPadding = 5
objTable.Cell(6,2).Select
objTable.Cell(6,2).Borders.Enable = True
'objSelection.Font.Name ="Arial"
'objSelection.Font.Size = 6 
'objselection.Font.Bold = False

'Original glyph insert
'objSelection.TypeParagraph()
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Facebook96.png"),  "https://www.facebook.com/pages/West-Chester-Protective-Gear/235840629898043", , , "Find us on Facebook")'
'objSelection.TypeText " "
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Twitter96.png"), "https://twitter.com/westchestergear", , , "Follow us on Twitter") 
'objSelection.TypeText " "
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Instagram96.png"), "https://www.instagram.com/westchesterprotective/", , , "Follow us on Instagram")
'objSelection.TypeText " "
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\LinkedIn96.png"), "http://www.linkedin.com/company/west-chester-holdings", , , "") 
'objSelection.TypeText " "
'Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Youtube96.png"), "https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube")
'objSelection.TypeParagraph()

'New Method puts image in the reverse order.
Set objShape = objTable.Cell(6,2).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Youtube96.png")
objdoc.Hyperlinks.Add objShape,"https://www.youtube.com/user/WestChesterGloves/", , , "West Chester Protective Gear on You Tube"
Set objShape = objTable.Cell(6,2).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\LinkedIn96.png")
objdoc.Hyperlinks.Add objShape,"http://www.linkedin.com/company/west-chester-holdings", , , ""
Set objShape = objTable.Cell(6,2).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Instagram96.png")
objdoc.Hyperlinks.Add objShape,"https://www.instagram.com/westchesterprotective/", , , "Follow us on Instagram"
Set objShape = objTable.Cell(6,2).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Twitter96.png")
objdoc.Hyperlinks.Add objShape,"https://twitter.com/westchestergear", , , "Follow us on Twitter"
Set objShape = objTable.Cell(6,2).Range.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\Facebook96.png")
objdoc.Hyperlinks.Add objShape,"https://www.facebook.com/pages/West-Chester-Protective-Gear/235840629898043", , , "Find us on Facebook"
objTable.Cell(6,2).BottomPadding = 5
objTable.Cell(6,2).Select
objSelection.Shading.Texture = wdTexture100Percent
objSelection.Shading.BackgroundPatternColor = wdBlack
objSelection.ParagraphFormat.Alignment = 2
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter'

'Call Me Tag
objTable.Cell(7,1).Select
objTable.Cell(7,1).Borders.Enable = False
ObjTable.Cell(7,1).BottomPadding = 0
objTable.Cell(7,1).LeftPadding = 0
objTable.Cell(7,1).RightPadding= 0
objTable.Cell(7,1).TopPadding = 0

'Set myRange = ActiveDocument.Range(Tables(1).Cell(8,1).Range.Start, Tables(1).Cell(8,1).Range.End)
'objTable.Cell.Borders.OutsideLineStyle = 
objTable.Cell(7,1).Borders.OutsideLineStyle =1
objTable.Cell(7,1).Borders.OutsideColor = 0

objTable.Cell(7,1).Borders(-1).LineStyle = 1
	objTable.Cell(7,1).Borders(-2).LineStyle = 0
    objTable.Cell(7,1).Borders(-3).LineStyle = 0
    objTable.Cell(7,1).Borders(-4).LineStyle = 0
    objTable.Cell(7,1).Borders(-7).LineStyle = 0
    objTable.Cell(7,1).Borders(-8).LineStyle = 0
    objTable.Cell(7,1).Borders(-1).LineWidth = 8
    objTable.Cell(7,1).Borders(-1).Color = 0
objSelection.Font.Name ="Arial"
objSelection.Font.Size = 6 
objselection.Font.Bold = False
objSelection.ParagraphFormat.Alignment = wdAlignParagraphCenter
objSelection.Cells.VerticalAlignment = wdCellAlignVerticalCenter
objSelection.TypeText " "
Set objLink = objSelection.Hyperlinks.Add(objSelection.InlineShapes.AddPicture("\\wcmktg.com\NETLOGON\Outlook\IMG\2018\CallMe96.png"), "http://www.westchesterprotects.com/stand-out-on-the-job-with-hi-viz-apparel/", , , "Customize your Hi-Viz apparel here at West Chester Protective Gear")

objTable.Select
objSelection.EndKey 6

If len(strAssistant) > 1 then
	objSelection.ParagraphFormat.Alignment = 0
	Set objUserAssist = GetObject("LDAP://" & strAssistant)
		strAsGiven = objuserAssist.givenName 
		strAsSurname = objuserAssist.sn 
		strAsFax = objuserAssist.facsimileTelephoneNumber 
		strAsMobile = objuserAssist.mobile 
		strAsTitle = objUserAssist.Title 
		strAsDepartment = objUserAssist.Department 
		strAsEmail =objuserAssist.mail 
		strAsExt = objuserAssist.ipPhone 
		strAsSkype =objuserAssist.pager 
		strAsEmailTEXT = "Email: " 
	objSelection.Font.Name = "Arial" 
	objSelection.Font.Size = 9 
	objselection.Font.Bold = True 
	objSelection.Font.Color = RGB (000,000,000)
	objSelection.ParagraphFormat.Alignment = 0 
	objSelection.TypeParagraph()
	objSelection.Font.Bold = True 
	objSelection.TypeText strAsTitle & ": " & strAsGiven & " " & strAsSurname
	objSelection.ParagraphFormat.Alignment = 0
	objSelection.TypeParagraph()
	objSelection.Font.Bold = True
	objSelection.Font.Size = 9 
	objSelection.TypeText "Email: "
	Set objLink = objSelection.Hyperlinks.Add(objSelection.Range, "mailto: " & strAsEmail, , , strAsEmail) 
		objLink.Range.Font.Name = "arial" 
		objLink.Range.Font.Size = 9 
		objLink.Range.Font.Bold = False
	objSelection.TypeParagraph() 
	objSelection.Font.Bold = True
	objSelection.Font.Size = 9 
	objSelection.TypeText "Office: "
	objSelection.Font.Bold = True
	objSelection.TypeText "800.647.1900 ext. " & strAsExt
		
End if

' ########### Tells outlook to use this signature for new messages and replys. Signature is called Email Signature. 
Set objSelection = objDoc.Range() 
objSignatureEntries.Add "WCPG_40th_Corporate", objSelection 
objSignatureObject.NewMessageSignature = "WCPG_40th_Corporate" 
objSignatureObject.ReplyMessageSignature = "WCPG_40th_Corporate" 
 
objDoc.Saved = True 
objWord.Quit